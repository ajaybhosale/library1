#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>phnx-dit</id>\
  <username>${MAVEN_USERNAME_DEV}</username>\
  <password>${MAVEN_PASSWORD_DEV}</password>\
 </server>\
 <server>\
  <id>phnx-sit</id>\
  <username>${MAVEN_USERNAME_DEV}</username>\
  <password>${MAVEN_PASSWORD_DEV}</password>\
 </server>\
 <server>\
  <id>phnx-uat</id>\
  <username>${MAVEN_USERNAME_DEV}</username>\
  <password>${MAVEN_PASSWORD_DEV}</password>\
 </server>\ 
 <server>\
  <id>phnx-preprod</id>\
  <username>${MAVEN_USERNAME_PREPROD}</username>\
  <password>${MAVEN_PASSWORD_PREPROD}</password>\
  </server>\
  <server>\
  <id>phnx-prod</id>\
  <username>${MAVEN_USERNAME_PROD}</username>\
  <password>${MAVEN_PASSWORD_PROD}</password>\
</server>" /usr/share/maven/conf/settings.xml